﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Text;

namespace FileStreamMercadoLivre
{
    public class Operation
    {
        public void Start()
        {
            var imageUrl = "http://images.tcdn.com.br/img/img_prod/550447/figure_my_hero_academia_the_amazing_heroes_vol3_katsuki_bakugo_475_1_20200305185721.jpg";
            var imageByteArray = DownloadImage(imageUrl);

            var response = SendToMercadoLivre(imageByteArray);
        }

        public byte[] DownloadImage(string url)
        {
            var client = new RestClient(url);
            client.Timeout = -1;
            client.UserAgent = "hub2b";
            var rrequester = new RestRequest(Method.GET);

            var response = client.Execute(rrequester);

            if (response.IsSuccessful == false)
                throw new Exception(response.Content);

            var imageByteArray = response.RawBytes;
            return imageByteArray;
        }

        public string SendToMercadoLivre(byte[] imageBytes)
        {
            var random = new Random();
            string randomImageName = $"image-{random.Next(1, 999999999)}.jpg";

            var client = new RestClient("https://api.mercadolibre.com/pictures/items/upload");
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.AddHeader("Authorization", "Bearer APP_USR-2511322507974679-011517-71fa7b4f2c53b886bbef5d4baf9c8334-330036488");
            request.AddHeader("content-type", "multipart/form-data");
            request.AlwaysMultipartFormData = true;
            request.AddFileBytes("file", imageBytes, randomImageName);
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
    }
}
